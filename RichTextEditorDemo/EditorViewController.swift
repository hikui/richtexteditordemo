//
//  EditorViewController.swift
//  RichTextEditorDemo
//
//  Created by Henry Heguang Miao on 27/4/17.
//  Copyright © 2017 Henry Miao. All rights reserved.
//

import UIKit
import RichEditorView

protocol EditorViewControllerDelegate: class {
    func didEdit(html: String)
}

class EditorViewController: UIViewController {

    @IBOutlet weak var editorView: RichEditorView!
    weak var delegate: EditorViewControllerDelegate?
    var toolbar: RichEditorToolbar!

    var html: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
        toolbar.options = RichEditorDefaultOption.all
        toolbar.editor = editorView
        editorView.inputAccessoryView = toolbar
        editorView.html = html
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        toolbar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func done() {
        self.delegate?.didEdit(html: editorView.html)
        self.navigationController?.popViewController(animated: true)
    }

}
