//
//  ViewController.swift
//  RichTextEditorDemo
//
//  Created by Henry Heguang Miao on 27/4/17.
//  Copyright © 2017 Henry Miao. All rights reserved.
//

import UIKit

class ViewController: UIViewController, EditorViewControllerDelegate {

    let html = "<p><b>Secure Upload of Manual Donations </b></p><p> </p><p>Just a reminder to all our fundraisers that there is now a secure upload function available in the portal for manual donation forms that need to be processed. For security reasons, this should be the way you send us manual donation forms from herein.</p><p> </p><div class=\"whatever\"><p>Hi there.</p></div><p>We&#39;ve outlined the steps for you at https://asf.org.au/resources/secure-upload-manual-donations/, however if you have any questions please contact your Partnership Manager."

    @IBOutlet weak var originalTextView: UITextView!
    @IBOutlet weak var afterTextView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.originalTextView.text = html
        self.afterTextView.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destination = segue.destination as? EditorViewController {
            destination.html = self.originalTextView.text
            destination.delegate = self
        }
    }

    func didEdit(html: String) {
        self.afterTextView.text = html
    }

}

